# lambdahttp

A simple [Go](https://golang.org/) library that wraps
[aws-lambda-go](https://github.com/aws/aws-lambda-go) to use an interface more
faithful to the [net/http](https://pkg.go.dev/net/http) standard. This makes it
easier to set cookies, use redirects, and generally plays more likely with the
vast number of Go libraries that work with HTTP.

## Usage

``` go
package main

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/jD91mZM2/lambdahttp"
)

func handler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, World")
}

func main() {
	lambdahttp.Start(handler)
}
```

See the `example/` directory for a working example using this library. You can
deploy it easily via:

- `aws configure` to log in
- `terraform init` to initialise modules
- `terraform apply` to deploy or update your function
