module api_gateway {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = "lambdahttp-example"
  description   = "API for Lambda HTTP Function"
  protocol_type = "HTTP"

  create_api_domain_name = false

  cors_configuration = {
    allow_headers = ["*"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  # Routes and integrations
  integrations = {
    "ANY /" = {
      lambda_arn = module.lambda_function.this_lambda_function_arn
    }
  }
}

output endpoint {
  value = module.api_gateway.this_apigatewayv2_api_api_endpoint
}
