module lambda_function {
  depends_on = [
    null_resource.gobuild
  ]

  source = "terraform-aws-modules/lambda/aws"

  function_name = "lambdahttp-example"
  description   = "Simple Lambda HTTP Function"
  handler       = "lambdahttp-example"
  runtime       = "go1.x"

  source_path = "./bin"

  publish = true

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.this_apigatewayv2_api_execution_arn}/*/*/*"
    }
  }
}
