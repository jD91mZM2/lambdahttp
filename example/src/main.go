package main

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/jD91mZM2/lambdahttp"
)

func handler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:  "cookie",
		Value: "Hello, I'm a cookie!",
		Path:  "/",
	})
	fmt.Fprintln(w, "Hello, World! I set a cookie :D")

	prev, err := r.Cookie("cookie")
	if err == nil {
		fmt.Fprintln(w, "The previous cookie value is:", prev.Value)
	} else {
		fmt.Fprintln(w, "Could not get previous cookie:", err.Error())
	}

	fmt.Fprintln(w, "URL Query:", r.URL.Query())
}

func main() {
	lambdahttp.Start(handler)
}
