module gitlab.com/jD91mZM2/lambdahttp/example

go 1.16

require github.com/aws/aws-lambda-go v1.22.0 // indirect
require gitlab.com/jD91mZM2/lambdahttp v0.0.0 // indirect

replace gitlab.com/jD91mZM2/lambdahttp => ../..
