resource null_resource gobuild {
  // Go builds are so fast anyway
  triggers = {
    always_trigger = timestamp()
  }

  provisioner local-exec {
    command = "${path.module}/build.sh"
  }
}
