#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "${BASH_SOURCE[@]}")"

mkdir -p bin

export GOOS=linux
export GOARCH=amd64
export CGO_ENABLED=0

pushd src &> /dev/null
go build -o ../bin/lambdahttp-example .
popd &> /dev/null
