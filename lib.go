package lambdahttp

import (
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type stringWriterInner struct {
	header http.Header
	status int
	writer strings.Builder
}
type stringWriter struct {
	inner *stringWriterInner
}

func (w stringWriter) Header() http.Header {
	return w.inner.header
}
func (w stringWriter) WriteHeader(status int) {
	if w.inner.status == 0 {
		w.inner.status = status
	}
}
func (w stringWriter) Write(body []byte) (int, error) {
	w.WriteHeader(http.StatusOK)
	if len(w.inner.header.Get("Content-Type")) == 0 {
		len := len(body)
		if len > 512 {
			len = 512
		}
		w.inner.header.Add("Content-Type", http.DetectContentType(body[:len]))
	}
	return w.inner.writer.Write(body)
}

// Wrap a lambda handler function. It will make the function input
// `http.ResponseWriter` and `http.Request` instead of
// `events.APIGatewayV2HTTPRequest`. You might be better off using
// lambdahttp.Start instead, which uses this function.
func WrapFunction(f func(ctx context.Context, w http.ResponseWriter, r *http.Request)) func(ctx context.Context, request events.APIGatewayV2HTTPRequest) (events.APIGatewayV2HTTPResponse, error) {
	return func(ctx context.Context, request events.APIGatewayV2HTTPRequest) (events.APIGatewayV2HTTPResponse, error) {
		protocol := request.RequestContext.HTTP.Protocol
		if protocol == "" {
			protocol = "HTTP/1.0"
		}

		major, minor, ok := http.ParseHTTPVersion(protocol)
		if !ok {
			return events.APIGatewayV2HTTPResponse{}, errors.New("Protocol was invalid: " + protocol)
		}

		headers := make(http.Header, len(request.Headers))
		for name, value := range request.Headers {
			headers.Add(name, value)
		}

		// For some reason, RawQueryString is empty for me
		query := make(url.Values, len(request.QueryStringParameters))
		for name, value := range request.QueryStringParameters {
			query.Add(name, value)
		}

		r := http.Request{
			Method: request.RequestContext.HTTP.Method,
			URL: &url.URL{
				Path:     request.RawPath,
				RawQuery: query.Encode(),
			},
			Proto:      request.RequestContext.HTTP.Protocol,
			ProtoMajor: major,
			ProtoMinor: minor,
			Header:     headers,
			// I need to support Go 1.14, otherwise I'd use
			// io.NopCloser > ioutil.NopCloser
			Body:          ioutil.NopCloser(strings.NewReader(request.Body)),
			ContentLength: int64(len(request.Body)),
		}
		w := stringWriter{
			inner: &stringWriterInner{
				header: make(http.Header),
			},
		}

		f(ctx, w, &r)

		return events.APIGatewayV2HTTPResponse{
			Body:              w.inner.writer.String(),
			MultiValueHeaders: w.inner.header,
			StatusCode:        w.inner.status,
		}, nil
	}
}

// Wraps `lambda.Start`, but instead of taking `events.APIGatewayV2HTTPRequest`
// the function takes `http.Request`.
func Start(f func(ctx context.Context, w http.ResponseWriter, r *http.Request)) {
	lambda.Start(WrapFunction(f))
}
